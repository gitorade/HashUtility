unit Unit1;

{$mode objfpc}{$H+}{$J-}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, LCLType, ComCtrls,
  EditBtn, StdCtrls, Clipbrd, ExtCtrls, HlpHashFactory, HlpIHash;

const
  BUFFER_SIZE = 1024*1024;

type
  TFileBuffer = array[0 .. BUFFER_SIZE - 1] of byte;

//================================================================================================//
//                                      TCalcFileHash                                             //                         
//================================================================================================//
type
  TCalcFileHash = class(TThread)
  public
    constructor Create(createSuspended: Boolean; fileName: String; hashFn: IHash);
    
    function GetProgress: Integer;
    
  protected
    procedure Execute; override;
    
  private
    fileStream: TFileStream;
    filePath: String;
    hash: IHash;
    hashStr: String;
    progress: Integer;
    
    procedure OnFinished;
  end;

//================================================================================================//
//                                         TForm1                                                 //                         
//================================================================================================//
type
  TForm1 = class(TForm)
    BuPasteHash: TButton;
    Button1: TButton;
    EdMD5: TEdit;
    LaSHA512: TCheckBox;
    LaSHA1: TCheckBox;
    EdFile: TFileNameEdit;
    EdHash: TEdit;
    EdSHA512: TEdit;
    EdSHA1: TEdit;
    EdSHA256: TEdit;
    GbHashes: TGroupBox;
    LaHash: TLabel;
    LaSHA256: TCheckBox;
    LaMD5: TCheckBox;
    LaVerified: TLabel;

    LaFile: TLabel;
    Progressbar: TProgressBar;
    TimUpdateProgress: TTimer;
    procedure BuPasteHashClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure EdFileAcceptFileName(Sender: TObject; var Value: String);
    procedure Compute(const fileName: string);
    procedure FormCreate(Sender: TObject);
    procedure FormDropFiles(Sender: TObject; const FileNames: array of String);
    procedure TimUpdateProgressTimer(Sender: TObject);
    procedure CheckHashes;
  private
    threadSHA1: TCalcFileHash;
    threadSHA256: TCalcFileHash;
    threadSHA512: TCalcFileHash;
    threadMD5: TCalcFileHash;
    numRunningThreads: Integer;
    numLaunchedThreads: Integer;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

//================================================================================================//
//                                      TCalcFileHash                                             //                         
//================================================================================================//
constructor TCalcFileHash.Create(createSuspended: Boolean; fileName: String; hashFn: IHash);
begin
  inherited Create(createSuspended);
  filePath := fileName;
  fileStream := nil;
  hash := hashFn;
  progress := 0;
end;

procedure TCalcFileHash.Execute;
var
  buffer: TFileBuffer;
  bytesRead: LongInt;
begin
  if FileExists(filePath) then begin
    fileStream := TFileStream.Create(filePath, fmOpenRead or fmShareDenyWrite);
    try
      hash.Initialize();
      while (not Terminated) and (fileStream.Position < fileStream.Size) do begin
        bytesRead := fileStream.Read(buffer, Sizeof(buffer));
        hash.TransformUntyped(buffer, bytesRead);
        progress := Round(100 * fileStream.Position / Real(fileStream.Size));
      end;
      hashStr := LowerCase(hash.TransformFinal.ToString());
    finally
      FreeAndNil(fileStream);
    end;
    Synchronize(@OnFinished);
  end;
  Dec(Form1.numRunningThreads);
end;

function TCalcFileHash.GetProgress: Integer;
begin
  Result := 100;
  if fileStream <> nil then
    Result := InterLockedExchangeAdd(progress, 0);
end;

procedure TCalcFileHash.OnFinished;
begin
  if Terminated then
    exit;

  if hashStr <> '' then begin
    if hash.GetName = 'TSHA1' then
      Form1.EdSHA1.Text := hashStr;
    if hash.GetName = 'TSHA2_256' then
      Form1.EdSHA256.Text := hashStr;
    if hash.GetName = 'TSHA2_512' then
      Form1.EdSHA512.Text := hashStr;
    if hash.GetName = 'TMD5' then
      Form1.EdMD5.Text := hashStr;
  end;
end;

//================================================================================================//
//                                         TForm1                                                 //                         
//================================================================================================//
procedure TForm1.BuPasteHashClick(Sender: TObject);
begin
  EdHash.Clear;
  EdHash.PasteFromClipboard;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  if EdFile.FileName = '' then
    ShowMessage('No input file specified.')
  else
    Compute(EdFile.FileName);
end;

procedure TForm1.EdFileAcceptFileName(Sender: TObject; var Value: String);
begin
  Compute(Value);
end;

procedure TForm1.Compute(const fileName: string);
var
  reply: Integer;
begin
  if numRunningThreads > 0 then begin
    if threadSHA1 <> nil then
      threadSHA1.Terminate;
    if threadSHA256 <> nil then
      threadSHA256.Terminate;
    if threadSHA512 <> nil then
      threadSHA512.Terminate;
    if threadMD5 <> nil then
      threadMD5.Terminate;
    
    Progressbar.Position := 0;
    LaVerified.Caption := '';
    Button1.Caption := 'Compute && Verify';
  end
  else begin
      Progressbar.Position := 0;
      EdHash.Font.Color := Font.Color;
      EdSHA1.Font.Color := Font.Color;
      EdSHA256.Font.Color := Font.Color;
      LaHash.Font.Color := Font.Color;
      LaSHA1.Font.Color := Font.Color;
      LaSHA256.Font.Color := Font.Color;
      LaVerified.Font.Color := Font.Color;
      
      reply := IDYES;
      if (EdSHA1.Text <> '') or (EdSHA256.Text <> '') or (EdSHA512.Text <> '') or (EdMD5.Text <> '') then
        reply := Application.MessageBox(
          'Would you like to recalculate the hashes?', 'Recalculate', MB_ICONQUESTION + MB_YESNO);

      if reply = IDYES then begin
        if LaSHA1.Checked then begin;
          Inc(numRunningThreads);
          threadSHA1 := TCalcFileHash.Create(False, fileName, THashFactory.TCrypto.CreateSHA1());
        end
        else
          EdSHA1.Clear;

        if LaSHA256.Checked then begin
          Inc(numRunningThreads);
          threadSHA256 := TCalcFileHash.Create(False, fileName, THashFactory.TCrypto.CreateSHA2_256());
        end
        else
          EdSHA256.Clear;

        if LaSHA512.Checked then begin
          Inc(numRunningThreads);
          threadSHA512 := TCalcFileHash.Create(False, fileName, THashFactory.TCrypto.CreateSHA2_512());
        end
        else
          EdSHA512.Clear;
        
        if LaMD5.Checked then begin
          Inc(numRunningThreads);
          threadMD5 := TCalcFileHash.Create(False, fileName, THashFactory.TCrypto.CreateMD5());
        end
        else
          EdMD5.Clear;

        numLaunchedThreads := numRunningThreads;
        if numLaunchedThreads > 0 then begin
          TimUpdateProgress.Enabled := true;
          Button1.Caption := 'Cancel';
        end;
      end
      else
        CheckHashes;
  end;
end;

procedure TForm1.FormDropFiles(Sender: TObject; const FileNames: array of String);
begin
  EdFile.FileName := FileNames[0];
  Compute(FileNames[0]);
end;

procedure TForm1.TimUpdateProgressTimer(Sender: TObject);
var
  totalProgress: Real;
  factor: Real;
begin
  if numRunningThreads = 0 then begin
    FreeAndNil(threadSHA1);
    FreeAndNil(threadSHA256);
    FreeAndNil(threadSHA512);
    FreeAndNil(threadMD5);
    TimUpdateProgress.Enabled := false;
    Form1.CheckHashes;
    Form1.Button1.Caption := 'Compute && Verify';
  end
  else begin
    factor := 1.0 / Real(numLaunchedThreads);
    totalProgress := 0;

    if (threadSHA1 <> nil) then
      totalProgress := totalProgress + factor * threadSHA1.GetProgress();
    if (threadSHA256 <> nil) then
      totalProgress := totalProgress + factor * threadSHA256.GetProgress();
    if threadSHA512 <> nil then
      totalProgress := totalProgress + factor * threadSHA512.GetProgress();
    if threadMD5 <> nil then
      totalProgress := totalProgress + factor * threadMD5.GetProgress();
    Form1.Progressbar.Position := Round(Form1.ProgressBar.Max * totalProgress * 0.01);
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ClientHeight := 480;
  ClientWidth := 640;
  numRunningThreads := 0;
  threadSHA1 := nil;
  threadSHA256 := nil;
  threadSHA512 := nil;
  threadMD5 := nil;
end;

procedure TForm1.CheckHashes;
var
  hashStr, sha256Str, sha1Str, sha512Str, md5Str: String;
  verified: Boolean;
begin
  hashStr := LowerCase(EdHash.Text);
  EdHash.Text := hashStr;
  sha1Str := LowerCase(EdSHA1.Text);
  sha256Str := LowerCase(EdSHA256.Text);
  sha512Str := LowerCase(EdSHA512.Text);
  md5Str := LowerCase(EdMD5.Text);
  verified := false;

  if (sha1Str <> '') and (hashStr = sha1Str) then begin
    Form1.LaSHA1.Font.Color := clGreen;
    Form1.EdSHA1.Font.Color := clGreen;
    Form1.LaVerified.Caption := 'Hash Matches SHA1!';
    verified := true;
  end
  else if (sha256Str <> '') and (hashStr = sha256Str) then begin
    Form1.LaSHA256.Font.Color := clGreen;
    Form1.EdSHA256.Font.Color := clGreen;
    Form1.LaVerified.Caption := 'Hash Matches SHA-256!';
    verified := true;
  end
  else if (sha512Str <> '') and (hashStr = sha512Str) then begin
    Form1.LaSHA512.Font.Color := clGreen;
    Form1.EdSHA512.Font.Color := clGreen;
    Form1.LaVerified.Caption := 'Hash Matches SHA-512!';
    verified := true;
  end
  else if (md5Str <> '') and (hashStr = md5Str) then begin
    Form1.LaMD5.Font.Color := clGreen;
    Form1.EdMD5.Font.Color := clGreen;
    Form1.LaVerified.Caption := 'Hash Matches MD5!';
    verified := true;
  end;

  if verified then begin
    Form1.LaVerified.Font.Color := clGreen;
    Form1.EdHash.Font.Color := clGreen;
  end
  else if hashStr <> '' then begin
    Form1.EdHash.Font.Color := clRed;
    Form1.LaHash.Font.Color := clRed;
    Form1.LaVerified.Font.Color := clRed;
    Form1.LaVerified.Caption := 'Hash mismatch!';
  end;
end;

end.

